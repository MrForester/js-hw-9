function renderList(items, parent = document.body) {
    const ul = document.createElement('ul');
    parent.appendChild(ul);
    
    for (let item of items) {
      const li = document.createElement('li');
      li.textContent = item;
      ul.appendChild(li);
    }
  }

const items = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
renderList(items);